from hypothesis import given, strategies as st

from src.package.module_a import a 
from src.package.module_b import b 

def test_a():
    result = a()
    assert result=="a"


@given(st.integers(min_value=51, max_value=100))
def test_b_success(num):
    result = b(num)
    assert result=="pass"

@given(st.integers(min_value=0, max_value=49))
def test_b_failure(num):
    result = b(num)
    assert result=="fail"

