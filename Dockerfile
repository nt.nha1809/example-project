FROM python:3.10.1-slim-buster
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
CMD [ "bash", "src/run.sh" ]